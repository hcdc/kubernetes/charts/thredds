# SPDX-FileCopyrightText: 2023 Helmholtz-Zentrum hereon GmbH
#
# SPDX-License-Identifier: CC0-1.0

global:
  # Global Parameters for the gitlab host from where to build the source
  # images
  buildSource:
    gitlabHost: codebase.helmholtz.cloud
    gitlabProject: null
    # a branch or tag to build from. defaults to the default branch (i.e. main)
    gitlabRef: null
    # the secret with the private key to access the project on gitlab
    gitlabSourceSecret: k8sgitlab
  # cluster where this chart should be deployed
  openshiftCluster: null
  manualDeployKey: false

# the origin of the docker image to build (should derive from tomcat:8.5-jdk11)
fromImage: null

# the origin of the docker image to build
nginx:
  # the origin of the docker image for the nginx container to build
  fromImage: null

# buildSource is a mapping with overrides of global.buildSource
buildSource:
  gitlabHost: null
  gitlabProject: null
  gitlabRef: null
  gitlabSourceSecret: null

# command is the command that the docker container shall run (has to be a list)
command: null

# shutdown is a boolean flag that suspends all cronjobs and deployments
# of this helm chart. This is a shortcut to the replicas.
shutdown: false

# secrets is a mapping of secret key to secret default. These values
# can be used to generate a secret that is used during the build.
buildSecrets: {}

# buildSecretName is the name of the secret to use during the build.
buildSecretName: null

# a mapping to specify where to push the image to.
buildTargets: {}
  # <key>: # the name for the secret (can be the namespace to use)
  #   cluster: <the cluster to push to (used to obtain the registry)
  #   namespace: <target k8s namespace>  # optional. when empty, <key> is used

# successfulBuildsHistoryLimit is the number of old successful builds to
# retain. When a BuildConfig is created, the 2 most recent successful builds
# are retained unless this value is set. If removed after the BuildConfig has
# been created, all successful builds are retained.
successfulBuildsHistoryLimit: 2

# failedBuildsHistoryLimit is the number of old failed builds to retain. When a
# BuildConfig is created, the 5 most recent failed builds are retained unless
# this value is set. If removed after the BuildConfig has been created, all
# failed builds are retained.
failedBuildsHistoryLimit: 2

# buildArgs is a mapping from the argument to the value to be used in the
# Dockerfile during the build process
buildArgs: {}

# buildResources are requests and limits for cpu and memory for the pods that
# build the image
buildResources: {}
  # limits:
  #   cpu: '1'
  #   memory: 1000Mi
  # requests:
  #   cpu: 50m
  #   memory: 50Mi

# custom triggers that can be used to trigger a build
buildTriggers: []
# - imageChange: {}
#   type: ImageChange

# directory in the git repository where the Dockerfile is located
contextDir: "docker/thredds"

# path to the Dockerfile, relative to the given contextDir. If empty,
# Dockerfile is used
dockerfilePath: null

# servicePorts is a mapping from port name to port and targetPort. Default
# ports are
#
#     default:
#       port: 8080
#
# If your want to omit a builtin port, set the `port` value to `null`
servicePorts: {}

# the name for objects that are created with this chart
baseName: thredds

# volumes is a mapping of volume name to mounting options.
volumes: {}
  # <some-name>:  # the volume name
  #   mountPath: /opt/app-root/src/<some-name>  # path where to mount in the container, optional
  #   storage: "1Gi"  # optional
  #   keep: true  # optional
  #   readOnly: true  # optional
  #   excludeFromBackup: false  # optional
  #   storageClassName: ""  # optional
  #   projected: {}  # alternative to creating a PVC, see https://docs.openshift.com/container-platform/4.10/nodes/containers/nodes-containers-projected-volumes.html
  #   volumeName: <some-pv>  # the name of a PersistentVolume, e.g. if you want to mount via NFS
  #   mountOnly: false  # optional, if the volume is created somewhere else
  #   accessModes:  # optional, list of access modes within the container. if omitted, we use ReadWriteMany
  #     - "ReadWriteMany"
# deploymentStrategy is the strategy used for rolling out new pods by the
# Deployment. Can be either RollingUpdate or Recreate. Recreate will shut down
# the existing deployment before creating a new one.
deploymentStrategy: Recreate

# resources are requests and limits for cpu and memory for this pod
resources: {}
  # limits:
  #   cpu: 1000m
  #   memory: 8000Mi
  # requests:
  #   cpu: 1000m
  #   memory: 4000Mi

# replicas is the number of pod you want your application to scale to
replicas: 1

# config is a mapping of config key to config value.
config: {}
  # CONFIG_KEY: "some value"

# useManyFilesPVC is a boolean. If true, we expect that the cluster-admin added
# a `manyfilespvc` serviceaccount. You should use this option when you expect
# that a PVC mounted on the container contains houndreds of thousands of files,
# it uses `seLinuxOptions: {type: spc_t}` in the `securityContext`.
#
# See https://access.redhat.com/solutions/6221251 for information on how this
# is implemented
useManyFilesPVC: false
