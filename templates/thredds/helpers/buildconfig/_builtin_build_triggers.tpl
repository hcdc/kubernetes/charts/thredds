
{{- define "thredds.builtinBuildTriggers" -}}
triggers:
  - imageChange: {}
    type: ImageChange
  - imageChange:
      from:
        kind: ImageStreamTag
        name: thredds-docker:latest
    type: ImageChange
{{- end }}
