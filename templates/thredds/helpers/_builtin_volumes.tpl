
{{- define "thredds.builtinVolumes" -}}
thredds-logs:
  mountPath: /usr/local/tomcat/content/thredds/logs/
  storage: "500Mi"
  readOnly: false
logs:
  mountPath: /usr/local/tomcat/logs/
  storage: "500Mi"
  readOnly: false
{{- end }}
