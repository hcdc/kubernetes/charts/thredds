
{{- define "thredds.builtinConfig" -}}
{{- $builtinResources := include "thredds.builtinResources" . | fromYaml }}
{{- $mergedResources := mustMergeOverwrite (dict) $builtinResources .Values.resources }}
{{- $maxLim := floor (mulf (regexFind "[0-9]+" $mergedResources.limits.memory) 0.95 ) }}
THREDDS_XMX_SIZE: {{ $maxLim }}{{ regexFind "[a-zA-Z]+" $mergedResources.limits.memory }}
{{- $minLim := floor (mulf (regexFind "[0-9]+" $mergedResources.requests.memory) 0.95 ) }}
THREDDS_XMS_SIZE: {{ $minLim }}{{ regexFind "[a-zA-Z]+" $mergedResources.requests.memory }}
{{- end }}
